# Pizza 365 CUSTOMER
- Đây là bản mô tả các chức năng trang đặt hàng pizza giao diện khách hàng của website nhà hàng pizza 365.
- Bao gồm các chức năng chính như sau.

## 🌟 Các chức năng của Pizza 365 CUSTOMER'

### 1. Chức năng kiểm tra đơn hàng

#### 🧱 Kiểm tra đơn hàng giúp bạn có thể rà soát lại thông tin đặt hàng của mình một lần nữa trước khi đặt hàng

-  Sau khi chọn cho mình Combo Pizza <strong>"Menu combo Pizza 365"</strong> và loại Pizza <strong>"Chọn loại pizza"</strong>, điền các thông tin cá nhân như họ tên, số điện thoại, email, và voucher (nếu có) ,... ta bấm vào nút <button style="background-color:darkblue ; color : white"> Kiểm tra đơn</button> để kiểm tra lại đơn hàng.
- Sau khi bấm nút một giao diện như dưới đây sẽ hiển thị

![CheckOrder](imgMarkdown/checkOrder.PNG)

- Chúng ta sẽ kiểm tra lại những thông tin cá nhân, và menu pizza mình đã đặt, xem đã khớp chưa, nếu khớp rồi chúng ta gửi đơn

### 2. Chức năng Gửi đơn hàng

#### 🧱 Sau khi kiểm tra đơn hàng kỹ càng thì bắt đầu gửi đơn

- Bấm nút <button style="background-color:orange ; color : black"> Gửi đơn</button> để có thể gửi đơn hàng
- Sau khi bấm nút, phần kiểm tra đơn hàng sẽ mất, thay vào đó một orderCode được gửi trả, khách hàng dùng orderCode này để kiểm tra đơn hàng của mình và nhận đơn

![SendOrder](imgMarkdown/SendOrder.PNG)

## 💻 Công nghệ đã dùng
+ Front-End:
    - 1. [Boostrap 4](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
    - 2. Javascript
+ Back-end:
+ KIT:
+ DB: